package br.com.projuris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.autoconfigure.domain.EntityScan;

//Autor: Gilberto Ewald
public class MyCalculo implements Calculo {

	@Override
	public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {
		// TODO Auto-generated method stub
		return resolveCustoCargo(funcionarios);

	}

	@Override
	public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {
		// TODO Auto-generated method stub
		return resolveCustoDepartamento(funcionarios);
	}

	public Map<String, BigDecimal> jogaPraMapCargo(List<Funcionario> lFuncionario) {
		return lFuncionario.stream().collect(Collectors.groupingBy(f -> f.getCargo(),
				Collectors.reducing(BigDecimal.ZERO, Funcionario::getSalario, BigDecimal::add)));
	}

	public Map<String, BigDecimal> jogaPraMapDepartamento(List<Funcionario> lFuncionario) {
		return lFuncionario.stream().collect(Collectors.groupingBy(f -> f.getDepartamento(),
				Collectors.reducing(BigDecimal.ZERO, Funcionario::getSalario, BigDecimal::add)));
	}

	public List<CustoDepartamento> resolveCustoDepartamento(List<Funcionario> lFuncionario) {

		return jogaPraMapDepartamento(lFuncionario).entrySet().stream()
				.map(e -> new CustoDepartamento((e.getKey()), e.getValue())).collect(Collectors.toList());
	}

	public List<CustoCargo> resolveCustoCargo(List<Funcionario> lFuncionario) {

		return jogaPraMapCargo(lFuncionario).entrySet().stream().map(e -> new CustoCargo((e.getKey()), e.getValue()))
				.collect(Collectors.toList());
	}

}
