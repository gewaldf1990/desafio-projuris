package br.com.projuris;

import java.math.BigDecimal;

public class CustoCargo {

	private String cargo;
	private BigDecimal custo;

	public CustoCargo(String cargo, BigDecimal custo) {
		this.cargo = cargo;
		this.custo = custo;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public BigDecimal getCusto() {
		return custo;
	}

	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cargo == null) ? 0 : cargo.hashCode());
		result = prime * result + ((custo == null) ? 0 : custo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustoCargo other = (CustoCargo) obj;
		if (cargo == null) {
			if (other.cargo != null)
				return false;
		} else if (!cargo.equals(other.cargo))
			return false;
		if (custo == null) {
			if (other.custo != null)
				return false;
		} else if (!custo.equals(other.custo))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CustoCargo [cargo=" + cargo + ", custo=" + custo + "]";
	}

}