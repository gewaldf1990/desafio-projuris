package br.com.projuris;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjurisApplicationTests {


	private Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal("1000.0"));
	private Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal("7000.70"));
	private Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal("10000.45"));
	private Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal("1300.9"));
	private Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal("7500"));
	private Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal("11000.0"));
	private Funcionario funcionario7 = new Funcionario("Estagiário", "Jurídico", new BigDecimal("700.4"));
	private Funcionario funcionario8 = new Funcionario("Assistente", "Jurídico", new BigDecimal("1800.90"));
	private Funcionario funcionario9 = new Funcionario("Gerente", "Jurídico", new BigDecimal("9500.50"));
	private Funcionario funcionario10 = new Funcionario("Diretor", "Jurídico", new BigDecimal("13000.0"));

	private List<Funcionario> listaFuncionarios = Arrays.asList(funcionario1, funcionario2, funcionario3, funcionario4,
			funcionario5, funcionario6, funcionario7, funcionario8, funcionario9, funcionario10);

	

	@Test
	public void pegarCustoPorDepartamento() {

		MyCalculo myCalculo = new MyCalculo();

		CustoDepartamento c1 = new CustoDepartamento("Jurídico", new BigDecimal("25001.80"));
		CustoDepartamento c2 = new CustoDepartamento("Financeiro", new BigDecimal("19800.9"));
		CustoDepartamento c3 = new CustoDepartamento("Administrativo", new BigDecimal("18001.15"));

		List<CustoDepartamento> lCustoDepartamentoTest = new ArrayList<CustoDepartamento>();
		lCustoDepartamentoTest.add(c1);
		lCustoDepartamentoTest.add(c2);
		lCustoDepartamentoTest.add(c3);

		assertEquals(lCustoDepartamentoTest, myCalculo.custoPorDepartamento(listaFuncionarios));

	}

	@Test
	public void pegarCustoPorCargo() {

		MyCalculo myCalculo = new MyCalculo();

		CustoCargo c1 = new CustoCargo("Estagiário", new BigDecimal("700.4"));
		CustoCargo c2 = new CustoCargo("Diretor", new BigDecimal("34000.45"));
		CustoCargo c3 = new CustoCargo("Gerente", new BigDecimal("24001.20"));
		CustoCargo c4 = new CustoCargo("Assistente", new BigDecimal("4101.80"));

		List<CustoCargo> lCustoCargoTest = new ArrayList<CustoCargo>();
		lCustoCargoTest.add(c1);
		lCustoCargoTest.add(c2);
		lCustoCargoTest.add(c3);
		lCustoCargoTest.add(c4);

		assertEquals(lCustoCargoTest, myCalculo.custoPorCargo(listaFuncionarios));

	}

	@Test
	public void findArrayTest() {
		int array1[] = { 4, 9, 3, 7, 8 };
		int subArray1[] = { 3, 7 };
		int array2[] = { 1, 3, 5 };
		int subArray2[] = { 1 };
		int array3[] = { 4, 9, 3, 7, 8, 3, 7, 1 };
		int subArray3[] = { 3, 7 };

		MyFindArray myFindArray = new MyFindArray();
		assertEquals(2, myFindArray.findArray(array1, subArray1));
		assertEquals(0, myFindArray.findArray(array2, subArray2));
		assertEquals(5, myFindArray.findArray(array3, subArray3));

		int array4[] = { 7, 8, 9 };
		int subArray4[] = { 8, 9, 10 };
		assertEquals(-1, myFindArray.findArray(array4, subArray4));
		

	}

	@Test
	public void findCharTest() {

		String s1 = "reembolsar";
		String s2 = "stress";
		String s3 = "arara";

		MyFindChar myFindChar = new MyFindChar();
		assertEquals('m', myFindChar.findChar(s1));
		assertEquals('t', myFindChar.findChar(s2));
		assertEquals(' ', myFindChar.findChar(s3));

	}

}
