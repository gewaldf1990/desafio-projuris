package br.com.projuris.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.projuris.Funcionario;
import br.com.projuris.MyCalculo;
import br.com.projuris.MyFindArray;
import br.com.projuris.MyFindChar;

@RestController
@RequestMapping("/executaAtividades")
public class ExecutarWS {
	
	
	@GetMapping("/findChar")
	public ResponseEntity findChar(){
		MyFindChar myFindChar = new MyFindChar();
		char[] a = "abcadd".toCharArray();
		char b = 'd';
		return ResponseEntity.ok(myFindChar.pegarSubArray(a,'d'));
	}
	
	
	@GetMapping("/findArray")
	public ResponseEntity findArray(){
		MyFindArray myFindArray = new MyFindArray();
		int[] a = {4,9,3,7,8,3,7,1};
		int[] b = {3,7};
		return ResponseEntity.ok(!(a==null || b==null) ? myFindArray.pegarSubArray(a,b) : -1);
	}
	
	
	@GetMapping("/custoDepartamento")
	public ResponseEntity<?> custoDepartamento(){
		MyCalculo myCalculo = new MyCalculo();
		Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
		Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
		Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
		Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
		Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
		Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
		Funcionario funcionario7 = new Funcionario("Estagi�rio", "Jur�dico", new BigDecimal(700.4));
		Funcionario funcionario8 = new Funcionario("Assistente", "Jur�dico", new BigDecimal(1800.90));
		Funcionario funcionario9 = new Funcionario("Gerente", "Jur�dico", new BigDecimal(9500.50));
		Funcionario funcionario10 = new Funcionario("Diretor", "Jur�dico", new BigDecimal(13000.0));
		List<Funcionario> listaFuncionario = new ArrayList<>();
		listaFuncionario.add(funcionario1);
		listaFuncionario.add(funcionario2);
		listaFuncionario.add(funcionario3);
		listaFuncionario.add(funcionario4);
		listaFuncionario.add(funcionario5);
		listaFuncionario.add(funcionario6);
		listaFuncionario.add(funcionario7);
		listaFuncionario.add(funcionario8);
		listaFuncionario.add(funcionario9);
		listaFuncionario.add(funcionario10);
		return ResponseEntity.ok(myCalculo.resolveCustoDepartamento(listaFuncionario));
		
	}
	
	
	@GetMapping("/custoCargo")
	public ResponseEntity<?> custoCargo(){
		MyCalculo myCalculo = new MyCalculo();
		Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
		Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
		Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
		Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
		Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
		Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
		Funcionario funcionario7 = new Funcionario("Estagi�rio", "Jur�dico", new BigDecimal(700.4));
		Funcionario funcionario8 = new Funcionario("Assistente", "Jur�dico", new BigDecimal(1800.90));
		Funcionario funcionario9 = new Funcionario("Gerente", "Jur�dico", new BigDecimal(9500.50));
		Funcionario funcionario10 = new Funcionario("Diretor", "Jur�dico", new BigDecimal(13000.0));
		List<Funcionario> listaFuncionario = new ArrayList<>();
		listaFuncionario.add(funcionario1);
		listaFuncionario.add(funcionario2);
		listaFuncionario.add(funcionario3);
		listaFuncionario.add(funcionario4);
		listaFuncionario.add(funcionario5);
		listaFuncionario.add(funcionario6);
		listaFuncionario.add(funcionario7);
		listaFuncionario.add(funcionario8);
		listaFuncionario.add(funcionario9);
		listaFuncionario.add(funcionario10);
		return ResponseEntity.ok(myCalculo.resolveCustoCargo(listaFuncionario));
	}

}