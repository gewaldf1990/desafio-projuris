package br.com.projuris;

import java.util.Arrays;

//Autor: Gilberto Ewald
public class MyFindArray implements FindArray {

	@Override
	public int findArray(int[] array, int[] subArray) {
		return !(array==null || subArray==null) ? pegarSubArray(array,subArray) : -1;
	}
	
	public static int pegarSubArray(int[] array, int[] subArray) {
		
		int ocorrencia = -1;
	    outer:
	    for (int i = 0; i <= array.length - subArray.length; ++i) {
	        for (int j = 0; j < subArray.length; ++j) {
	            if (array[i + j] != subArray[j]) {
	                continue outer;
	            }
	        }
	        ocorrencia = i;
	    }
	    return ocorrencia;
	}
	

}