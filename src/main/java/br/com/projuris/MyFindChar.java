package br.com.projuris;

//Autor: Gilberto Ewald
public class MyFindChar implements FindCharacter {

	@Override
	public char findChar(String word) {
		char[] a = word.toCharArray();

		for (char var : a) {
			if (pegarSubArray(a, var) == 1) {
				return var;
			}
		}
		return ' ';
	}

	public static int pegarSubArray(char[] a, char b) {
		int ocorrencia = 0;
		for (int i = 0; i <= a.length - 1; ++i) {
			if (a[i] == b)
				ocorrencia++;
		}
		return ocorrencia;
	}

}
